package GameOfLife;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Dimension;
import java.awt.CardLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.TimerTask;
import java.util.Timer;

/**
 * Represents ConwayGameOfLife. Helps to listen to the user input
 * and display a game board and user interfaces.
 * @author Yoon Jin Park
 * @author https://bitbucket.org/YJAJ/1801ict_project_4/src/master/
 * @version 2.3
 * @since 1.0
 */

public class ConwayGameOfLife extends JPanel
{
    private static final int gap = 1;
    private static final Color bg = new Color(102, 0, 153);
    private static int [][] passedGeneration;
    private static int [][] pausedGeneration;
    private static Timer timer;

    /**
     * Runs the main method for the Game Of Life package.
     * Displays a game board that has the graphical grid cells and user interfaces.
     */
    public static void main(String[] args)
    {
        SwingUtilities.invokeLater(ConwayGameOfLife::runGui);
    }

    /**
     * Calls and initialise user interface class of ConwayGameOfLife.
     * Each cell in the grid has MouseLister so that when the cell is clicked,
     * the state of the cell changes and updates the graphics. This action also
     * creates a collection of the state of the cells in a current grid.
     * This is a constructor method for ConwayGameOfLife class.
     * @param side  user input of both the width and height of grid cells on a initial game board
     */
    private ConwayGameOfLife(int side)
    {
        //create original generation with the size of sides
        JPanel [][] placeHolder = new JPanel[side][side];
        setPreferredSize(new Dimension(22*side, 22*side));
        passedGeneration = new int[side][side];
        setBackground(bg);
        setLayout(new GridLayout(side, side, gap, gap));

        for (int i = 0; i < side; i++) {
            for (int j = 0; j < side; j++)
            {
                placeHolder[i][j] = new JPanel();
                //placeHolder[i][j].setOpaque(true);
                placeHolder[i][j].setBackground(Color.black);
                placeHolder[i][j].setBorder(BorderFactory.createLineBorder(Color.black));
                add(placeHolder[i][j]);

                int cellSize = 20;
                final Cell cell = new Cell(i, j, cellSize);
                placeHolder[i][j].addMouseListener(new MouseAdapter()
                {
                    public void mouseClicked(MouseEvent e)
                    {
                        if (cell.getAlive()==0)
                        {
                            int xPos = cell.getX();
                            int yPos = cell.getY();
                            placeHolder[xPos][yPos].setBackground(new Color(204, 204, 0));
                        }
                        else
                        {
                            int xPos = cell.getX();
                            int yPos = cell.getY();
                            placeHolder[xPos][yPos].setBackground(Color.black);
                        }
                        cell.toggleAlive();
                        createOriginalGeneration(passedGeneration, cell);
                    }
                });
            }
        }
    }

    /**
     * Registers and generates a collection of the state of cells in the current grid with the user input
     * @param generation the collection of the state of the cells (to be specific, alive or dead)
     * @param cell Cell object having the information of x and y coordinates and the state of the cell
     */
    private void createOriginalGeneration(int[][] generation, Cell cell)
    {
        int xPosition = cell.getX();
        int yPosition = cell.getY();
        generation[xPosition][yPosition] = cell.getAlive();
    }

    /**
     * Cancel the auto scheduled timer when pause button is pressed.
     */
    private static void timerPause()
    {
        timer.cancel();
    }

    /**
     * Initialise a timer instance when play or restart button is pressed.
     */
    private static void timerRestart()
    {
        timer = new Timer();
    }

    /**
     * Initialise and run GUI. All user interfaces are also initialised in this method.
     * The set of user interfaces includes the buttons and the dialogues specified below:
     * <p>
     *     start button - main start button to create an initialised game board to get user inputs
     * </p>
     * <p>
     *     user input dialog - text dialog requesting an input of the size of a grid from user.
     *     only a positive integer is accepted (otherwise, exit the dialog and go back to the main start button).
     * </p>
     * <p>
     *     display user input dialog - text dialog displaying what user input was in the previous dialog
     * </p>
     * <p>
     *     play button - when a play button clicked, the user input is passed on to apply the rules of a survival,
     *     and the auto scheduled timer is instantiated to display next generation every one second
     * </p>
     * <p>
     *     restart button - when restart button is clicked, exit the current game board and go back to the start menu
     * </p>
     * <p>
     *     quit button - when quit button is clicked, exit the program
     * </p>
     * <p>
     *     pause button - when pause button is clicked, cancel the auto scheduling timer, save the state
     * </p>
     * <p>
     *     continue button - when continue button is clicked, load the saved generation from pause action,
     *     and reschedule the timer
     * </p>
     * <p>
     *     save button - when save button is clicked, cancel the auto scheduling timer, save the collective state to the file
     * </p>
     */
    private static void runGui()
    {
        JFrame frame = new JFrame("Conway's Game of Life");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel mainPanel;
        mainPanel = new JPanel(new CardLayout());
        mainPanel.setPreferredSize(new Dimension(500, 500));
        CardLayout cardLayout = (CardLayout) mainPanel.getLayout();

        // Creating start button and setting an initial grid to obtain user input with mouse click
        JButton startButton = new JButton("Click to start on Conway's game of life!");
        JPanel startPanel = new JPanel();
        String startMenu = "Start Menu";
        startPanel.setLayout(new GridBagLayout());
        GridBagConstraints s = new GridBagConstraints();
        startPanel.add(startButton, s);
        mainPanel.add(startPanel, startMenu);
        cardLayout.show(mainPanel, startMenu);
        startButton.addActionListener(e -> {
            String askingSide = JOptionPane.showInputDialog("How many squares do you want on each side?");
            int side = Integer.parseInt(askingSide);
            if (side >0)
            {
                int input = JOptionPane.showOptionDialog(frame, "Making grid cells with the side of " + side + '!', "Creating cells...", JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);
                if(input == JOptionPane.OK_OPTION)
                {
                    //add grid cells based on user input
                    JPanel gamePanel = new JPanel();
                    String firstMenu = "First Menu";
                    JPanel boardPanel = new JPanel();
                    String nextMenu = "Game Board";
                    gamePanel.setLayout(new GridBagLayout());
                    boardPanel.setLayout(new GridBagLayout());
                    ConwayGameOfLife grids = new ConwayGameOfLife(side);
                    gamePanel.add(grids);

                    //add option buttons to option panel, which is added to game panel
                    JPanel optionPanel = new JPanel();
                    optionPanel.setLayout(new GridBagLayout());
                    JButton playButton = new JButton("Play");
                    JButton restartButton = new JButton("Restart");
                    JButton quitButton = new JButton("Quit");
                    JButton pauseButton = new JButton("Pause");
                    JButton continueButton = new JButton("Continue");
                    JButton saveButton = new JButton("Save");

                    GridBagConstraints c = new GridBagConstraints();
                    c.fill = GridBagConstraints.HORIZONTAL;
                    c.gridx = 0;
                    c.gridy = 1;
                    optionPanel.add(playButton, c);
                    c.gridx = 1;
                    optionPanel.add(restartButton, c);
                    restartButton.setEnabled(false);
                    c.gridx = 2;
                    optionPanel.add(quitButton, c);
                    c.gridx = 0;
                    c.gridy = 2;
                    optionPanel.add(pauseButton, c);
                    pauseButton.setEnabled(false);
                    c.gridx = 1;
                    optionPanel.add(continueButton, c);
                    continueButton.setEnabled(false);
                    c.gridx = 2;
                    optionPanel.add(saveButton, c);
                    saveButton.setEnabled(false);
                    //option buttons at the bottom and reset the padding for the whole panel
                    c.gridx = 0;
                    c.gridy = 2;
                    gamePanel.add(optionPanel, c);
                    mainPanel.add(gamePanel, firstMenu);
                    cardLayout.show(mainPanel, firstMenu);

                    //when a play button clicked, the user input is passed on to apply the rules of a survival,
                    //and the auto scheduled timer is instantiated to display next generation every one second
                    playButton.addActionListener(e1 -> {
                            passedGeneration = Runner.runner(passedGeneration, side);
                            GameBoard updatedGrids = new GameBoard(passedGeneration, side);
                            boardPanel.add(updatedGrids);
                            playButton.setEnabled(false);
                            restartButton.setEnabled(true);
                            pauseButton.setEnabled(true);
                            boardPanel.add(optionPanel, c);
                            mainPanel.add(boardPanel, nextMenu);
                            cardLayout.show(mainPanel, nextMenu);

                            timerRestart();
                            TimerTask myTask = new TimerTask()
                            {
                                @Override
                                public void run()
                                {
                                    boardPanel.removeAll();
                                    passedGeneration = Runner.runner(passedGeneration, side);
                                    GameBoard updatedGrids = new GameBoard(passedGeneration, side);
                                    boardPanel.add(updatedGrids);
                                    boardPanel.add(optionPanel, c);
                                    boardPanel.revalidate();
                                    mainPanel.add(boardPanel, nextMenu);
                                    cardLayout.show(mainPanel, nextMenu);
                                }
                            };
                            timer.scheduleAtFixedRate(myTask ,1000 , 1000);
                    });
                    //when restart button is clicked, exit the current game board and go back to the start menu
                    restartButton.addActionListener(e12 -> {
                        timerPause();
                        gamePanel.removeAll();
                        boardPanel.removeAll();
                        cardLayout.show(mainPanel, startMenu);
                    });
                    //when quit button is clicked, exit the program
                    quitButton.addActionListener(e13 -> System.exit(0));
                    //when pause button is clicked, cancel the auto scheduling timer, save the state
                    pauseButton.addActionListener(e14 -> {
                        timerPause();
                        pausedGeneration = passedGeneration;
                        continueButton.setEnabled(true);
                        saveButton.setEnabled(true);
                        pauseButton.setEnabled(false);
                    });
                    //when continue button is clicked, load the saved generation from pause action,
                    //and reschedule the timer
                    continueButton.addActionListener(e15 -> {
                        passedGeneration = pausedGeneration;
                        timerRestart();
                        TimerTask myTask = new TimerTask()
                        {
                            @Override
                            public void run()
                            {
                                boardPanel.removeAll();
                                passedGeneration = Runner.runner(passedGeneration, side);
                                GameBoard updatedGrids = new GameBoard(passedGeneration, side);
                                boardPanel.add(updatedGrids);
                                playButton.setEnabled(false);
                                pauseButton.setEnabled(true);
                                saveButton.setEnabled(false);
                                boardPanel.add(optionPanel, c);
                                boardPanel.revalidate();
                                mainPanel.add(boardPanel, nextMenu);
                                cardLayout.show(mainPanel, nextMenu);
                            }
                        };
                        timer.scheduleAtFixedRate(myTask ,1000 , 1000);
                        continueButton.setEnabled(false);
                        pauseButton.setEnabled(true);
                    });
                    //when save button is clicked, cancel the auto scheduling timer, save the collective state to the file
                    saveButton.addActionListener(e16 -> {
                        timerPause();
                        pausedGeneration = passedGeneration;
                        String askingFileName = JOptionPane.showInputDialog(null, "Enter a file name below.", "Game Of Life");
                        askingFileName = askingFileName+".txt";
                        StringBuilder builder = new StringBuilder();
                        for (int i=0; i<side; ++i)
                        {
                            for (int j=0; j<side; ++j)
                            {
                                builder.append(pausedGeneration[i][j]);
                                if (j<side-1)
                                {
                                    builder.append(",");
                                }
                            }
                            builder.append("\n");
                        }
                        FileWriter file = null;
                        try {
                            file = new FileWriter(askingFileName);
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                        BufferedWriter writer = new BufferedWriter(file);
                        try {
                            writer.write(builder.toString());
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                        try {
                            writer.close();
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                        continueButton.setEnabled(true);
                        pauseButton.setEnabled(false);
                        JOptionPane.showOptionDialog(frame, "Saved!", "Save", JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);
                    });
                }
            }
            else
            {
                //provides error message when the user input is not a positive integer
                JOptionPane.showMessageDialog(frame, "That is not a valid number. Try again with a positive integer!");
            }
        });

        //add card layout of mainPanel to the frame and display the window
        frame.getContentPane().add(mainPanel);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
